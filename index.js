const express = require("express");

const app = express();

const port = 3000;

let users = [];

app.use(express.json())

app.use(express.urlencoded({extended:true}));


app.get("/home", (req, res)=>{
	res.send("This is home.")
});

app.get("/users", (req, res)=>{
	console.log(typeof users)
	res.send(users)
});

app.post("/users", (req, res)=>{
	users.push(req.body)
	console.log(users)
	res.send(`User ${req.body.firstName} has successfully  registered.`)
})


app.listen(port, ()=>{
	console.log(`server running at port ${port}`)
})

